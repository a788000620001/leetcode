class Solution:
    def summaryRanges(self, nums: List[int]) -> List[str]:
        if not nums:
            return None
        
        lower_bound = nums[0]
        upper_bound = nums[0]
        
        output = []
        for number in nums[1:]:
            if number -1 != upper_bound:
                if lower_bound == upper_bound:
                    output.append(f"{lower_bound}")
                else:
                    output.append(f"{lower_bound}->{upper_bound}")
                lower_bound = number
            upper_bound = number
            
        if lower_bound == upper_bound:
            output.append(f"{lower_bound}")
        else:
            output.append(f"{lower_bound}->{upper_bound}")
        return output
